#===============================================================================
#
# FILE: cache_warmer_d8.sh
#
# USAGE: ./cache_warmer_d8.sh <domain> <key>
#
# DESCRIPTION: Remotely run the cron task on the given Drupal site, then send a
#   head request to every page in the sitemap to warm the cache. The script
#   employs subshells to process multiple requests at a time, allowing the task
#   to complete more quickly.
#
# ARGUMENTS:
#   domain: The URL of the site to process
#   key: The external cron key of the site
#
# AUTHOR: Nigel Packer 2020
#
#===============================================================================

set -o errexit
set -o nounset
set -o pipefail

# Base URL from first parameter
readonly domain=$1

# Cron key from second paramter
readonly key=$2

# Cron request url
readonly request="https://${domain}/cron/${key}"

# Max simultaneous requests
readonly concurrency=8

# Sitemap URL
readonly sitemap="https://${domain}/sitemap.xml"

# Pattern to read page URL from sitemap
readonly pattern="${domain}[^<|\"]+"

echo "Running cron for: https://${domain}..."

# Perform cron request
curl $request \
  --silent \
  --verbose \
  --show-error \
  --fail \
  --connect-timeout 60 \
  --max-time 1800 \
  --retry 3 \
  --retry-delay 180

# Disable immediate exit on failure
set +o errexit

echo "Warming cache for: https://${domain}..."

count=1

# Iterate over each page in the sitemap
for page in $(curl --silent $sitemap | grep --extended-regexp --only-matching $pattern); do

  # Make up to {concurrency} simultaneous requests
  if ((count > concurrency)); then
    count=1
    wait
  else
    ((++count))
  fi

  # Make a HEAD request to the page. This avoid downloading the full page
  # content.
  (echo $page $(curl "https://${page}" \
    --silent \
    --head \
    --connect-timeout 10 \
    --max-time 10 \
    | grep --extended-regexp --only-matching "(HIT|MISS|no-cache)")) &

done
